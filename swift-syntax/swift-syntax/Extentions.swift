//
//  Extentions.swift
//  swift-syntax
//
//  Created by Alexander Borodulya on 8/7/18.
//  Copyright © 2018 Alexander Borodulya. All rights reserved.
//

import Foundation

extension Int {
    var gain10: Int { return self * 10 }
    var gain11: Int { return self * 11 }
}

func Extentions() {
    let testExtention10: Int = Int(10).gain10
    let testExtention11: Int = Int(10).gain11
    print("testExtention: \(testExtention10)")
    print("testExtention: \(testExtention11)")
    print("testExtention: \(Int(11).gain11)")
}
