//
//  Functions.swift
//  swift-syntax
//
//  Created by Alexander Borodulya on 8/3/18.
//  Copyright © 2018 Alexander Borodulya. All rights reserved.
//

import Foundation

func firstInSwift(arg: String) -> String {
    return "First+" + arg
}

func Functions() {
    print(firstInSwift(arg: "First"))
    print(firstInSwift(arg: "Second"))
}


