//
//  Generics.swift
//  swift-syntax
//
//  Created by Alexander Borodulya on 8/13/18.
//  Copyright © 2018 Alexander Borodulya. All rights reserved.
//

import Foundation

func swapValues(_ a: inout Int, _ b: inout Int) {
    print("called: swapValues(Int...)")
    let t = a
    a = b
    b = t
}

func swapValues<T>(_ a: inout T, _ b: inout T) {
    print("called: swapValues<T>")
    let t = a
    a = b
    b = t
}


func Generics () {
    var a = 10
    var b = 20
    
    print("a = \(a), b = \(b)")
    swapValues(&a, &b)
    print("a = \(a), b = \(b)\n")
    
    var d1 = 5.0
    var d2 = 7.5
    print("a = \(d1), b = \(d2)")
    swapValues(&d1, &d2)
    print("a = \(d1), b = \(d2)\n")
}
