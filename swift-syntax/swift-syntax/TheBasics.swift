//
//  TheBasics.swift
//  swift-syntax
//
//  Created by Alexander Borodulya on 8/7/18.
//  Copyright © 2018 Alexander Borodulya. All rights reserved.
//

import Foundation

func TheBasics() {
    
    //
    // Constants and variables
    //
    let constIntValue = 100
    var nonConstIntValue = 200
    
    //constIntValue = 1000 Cannot assign to value: 'constIntValue' is a 'let' constant
    nonConstIntValue = 2000
    
    // Even this possible in Swift
    var 😀 = "Smile"
    var π = "Math symbol"
    
    //
    // Types Annotations
    //
    var stringVariable: String
    var intVariable: Int
    
    stringVariable = "String Value"
    intVariable = 100
//    intVariable = 1.2 Cannot assign value of type 'Double' to type 'Int'
//    intVariable = "String as well" Cannot assign value of type 'String' to type 'Int'
    
    //
    // Print
    //
    print(constIntValue)
    print(nonConstIntValue)
    print(😀)
    print(π)
    print(stringVariable)
    print(intVariable)
    
    //
    // String interpolation
    //
    print("constIntValue: \(constIntValue)")
    print("constIntValue: \(constIntValue)", "Extra string", separator: "[THIS IS A SEPARATOR]")
    let someString: String
    someString = "someString with constIntValue: \(constIntValue)"
    print("someString: \(someString)")
    
    //
    // Comments
    //
    
    // This was easy
    
    /* This was easy
     as well */
    
    /* Comments
     /* Are */
     Awesome */
    
    //
    // Integers and Floats
    //
    func printIntBounds() {
        print("Int.min \(Int.min)")
        print("Int.max \(Int.max)")
        print("Int8.min \(Int8.min)")
        print("Int8.max \(Int8.max)")
        print("Int16.min \(Int16.min)")
        print("Int16.max \(Int16.max)")
        print("Int32.min \(Int32.min)")
        print("Int32.max \(Int32.max)")
        print("Int64.min \(Int64.min)")
        print("Int64.max \(Int64.max)")
    }
    
    func printUIntBounds() {
        print("UInt.min \(UInt.min)")
        print("UInt.max \(UInt.max)")
        print("UInt8.min \(UInt8.min)")
        print("UInt8.max \(UInt8.max)")
        print("UInt16.min \(UInt16.min)")
        print("UInt16.max \(UInt16.max)")
        print("UInt32.min \(UInt32.min)")
        print("UInt32.max \(UInt32.max)")
        print("UInt64.min \(UInt64.min)")
        print("UInt64.max \(UInt64.max)")
    }
    
    func printNumbersBounds() {
        printIntBounds()
        printUIntBounds()
    }
    
    printNumbersBounds()
    
    let intVar: Int = 1000
    let floatVar: Float = 1.2
    let doubleVar: Double = 1.2
    let uintVar: UInt64 = 1 << 40
    print("intVar: \(intVar)")
    print("floatVar: \(floatVar)")
    print("doubleVar: \(doubleVar)")
    print("uintVar: \(uintVar)")
    
    // Gonna print pi
    print("Float.pi: \(Float.pi)")
    print("Double.pi: \(Double.pi)")
    
    // Numeric Literals
    let decInt = 100
    let binInt = 0b100
    let octInt = 0o100
    let hexInt = 0x100
    
    print("decInt: \(decInt)")
    print("binInt: \(binInt)")
    print("octInt: \(octInt)")
    print("hexInt: \(hexInt)")
    
    let doubleExpoPosValue = 1.125e3
    let doubleExpoNegValue = 1.125e-3
    let a1 = 1.02
    let a2 = 1.02e2
    let a3 = 0x1.02p2
    print("doubleExpoPosValue \(doubleExpoPosValue)")
    print("doubleExpoNegValue \(doubleExpoNegValue)")
    print("a1 \(a1)")
    print("a2 \(a2)")
    print("a3 \(a3)")
    
    let allowedLiteral = 0001.125
    let allowedLiteralToo =  1_000_000_000_000
    let allowedLiteralAsWell =  1_000_000.000_999
    print("allowedLiteral: \(allowedLiteral)")
    print("allowedLiteralToo: \(allowedLiteralToo)")
    print("allowedLiteralAsWell: \(allowedLiteralAsWell)")
    
    //
    // Numeric Type Conversion
    //
    
    // Integer conversion
    let ui8var: UInt8 = 255
    let ui16var: UInt16 = 745
//    let ui8var2: UInt8 = ui8var - UInt8(UInt16(1)) // Warning: Immutable value 'ui8var2' was never used; consider replacing with '_' or removing it
//    let u8var3: UInt8 = -1 // Error: Negative integer '-1' overflows when stored into unsigned type 'UInt8'
//    let u8var3: UInt8 = UInt8.max + 1 // Error: Arithmetic operation '255 + 1' (on type 'UInt8') results in an overflow
//    let u8var3: UInt8 = UInt8(-1) // Error: Thread 1: Fatal error: Negative value is not representable
//    let u8var3: UInt8 = UInt8(Int8(-1)) // Run-time: Error: Thread 1: Fatal error: Negative value is not representable
//    let u16var3 = ui16var + ui8var // Error: Binary operator '+' cannot be applied to operands of type 'UInt16' and 'UInt8'
    let u16var3 = ui16var + UInt16(ui8var) // Ok
    print("u16var3: \(u16var3)")
    
    // Integer and Floating-Point Conversion
    let three = 3
    let someDouble = 0.14159
    let piValue = Double(three) + someDouble
    
    print("piValue: \(piValue)")
}
