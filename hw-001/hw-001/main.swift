//
//  main.swift
//  hw-001
//
//  Created by Alexander Borodulya on 8/9/18.
//  Copyright © 2018 Alexander Borodulya. All rights reserved.
//

import Foundation

let MinNumber: Int = 0
let MaxNumber: Int = 30

func fizzBuzz(arg: Int) -> String {
    var result = ""
    
    if (arg % 15) == 0 {
        result = "FizzBuzz"
    } else if (arg % 5) == 0 {
        result = "Buzz"
    } else if (arg % 3) == 0 {
        result = "Fizz"
    }
    
    return result
}

func fizzBuzzAdvanced(arg: Int) -> String {
    var result: String = ""
    
    let divBy3 = arg % 3 == 0
    let divBy5 = arg % 5 == 0
    let divBy15 = arg % 15 == 0
    
    // let divBy15 = Bool(arg % 15) // Error: Cannot invoke initializer for type 'Bool' with an argument list of type '(Int)'
    
    if (divBy3) {
        result = "Fizz"
    }
    
    if (divBy5) {
        result = result.isEmpty ? "Buzz" : result + "/Buzz"
    }
    
    if (divBy15) {
        result = result.isEmpty ? "FizzBuzz" : result + "/FizzBuzz"
    }
    
    return result
}

func testFizzBuzz() {
    for i in MinNumber...MaxNumber {
        print("\(#function): i(\(i)) = \(fizzBuzz(arg: i))")
    }
}

func testFizzBuzzAdvanced() {
    for i in MinNumber...MaxNumber {
        print("\(#function): i(\(i)) = \(fizzBuzzAdvanced(arg: i))")
    }
}

func hw001() {
    testFizzBuzz()
    testFizzBuzzAdvanced()
}

// Run Home Work 001...
hw001()
