//
//  hw002.swift
//  hw-002
//
//  Created by Alexander Borodulya on 8/11/18.
//  Copyright © 2018 Alexander Borodulya. All rights reserved.
//

import Foundation

func binaryString(value: UInt8) -> String {
    print("called: binaryString(:UInt8)")
    var output: String = ""
    
    let typeSize: UInt8 = UInt8(MemoryLayout<Int8>.size)
    var testBit: UInt8 = 1 << UInt8((typeSize * 8) - 1)
    
    while testBit > 0 {
        
        // QUESTION: Good or bad following type convertion?
        // let hasBit: Bool = NSNumber(value: topBit & testValue) as! Bool
        
        let hasBit: Bool = NSNumber(value: testBit & value).boolValue
        output += hasBit ? "1" : "0"
        testBit >>= 1
    }
    
    return output
}

func binaryString<T>(value: T) -> String {
    print("called: binaryString(:T)")
    var output: String = ""
    let typeSize: Int = MemoryLayout<T>.size
    let maxBit = typeSize * 8 - 1
    var testBit = 1 << maxBit
    
    while testBit > 0 {
        
        // QUESTION: Good or bad following type convertion?
        // let hasBit: Bool = NSNumber(value: topBit & testValue) as! Bool
        
//        let hasBit: Bool = NSNumber(value: testBit & value).boolValue
//        output += hasBit ? "1" : "0"
        testBit >>= 1
    }
    
    return output
}

func binaryStringNoPadding(value: UInt8) -> String {
    var output: String = ""
    
    let typeSize: UInt8 = UInt8(MemoryLayout<Int8>.size)
    var testBit: UInt8 = 1 << UInt8((typeSize * 8) - 1)
    
    while testBit > 0 {
        // let hasBit: Bool = NSNumber(value: topBit & testValue) as! Bool
        let hasBit: Bool = NSNumber(value: testBit & value).boolValue
        
        // No padding with zeros
        if !output.isEmpty || hasBit {
            output += hasBit ? "1" : "0"
        }
        
        testBit >>= 1
    }
    
    return output
}
