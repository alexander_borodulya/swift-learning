//
//  main.swift
//  hw-002
//
//  Created by Alexander Borodulya on 8/10/18.
//  Copyright © 2018 Alexander Borodulya. All rights reserved.
//

import Foundation

func testTypeRange() {
    for i in (UInt8.min...UInt8.max) {
        let s = String(i, radix: 2)
        let bs = binaryString(value: UInt8(i))
        let bsnp = binaryStringNoPadding(value: UInt8(i))
        let passed = s == bsnp
        print("Value: \(i) s: \(s) bs: \(bs) bsnp: \(bsnp) status: \(passed)")
    }
}

func testSingleVariables() {
    let i8: Int8 = 100
    let ui8: UInt8 = 101
    let i16: Int16 = 102
    let ui16: UInt16 = 103
    let i32: Int32 = 104
    let ui32: UInt32 = 105
    let i64: Int64 = 106
    let ui64: UInt64 = 107
    
    let i8s = binaryString(value: i8)
    let ui8s = binaryString(value: ui8)
    let i16s = binaryString(value: i16)
    let ui16s = binaryString(value: ui16)
    let i32s = binaryString(value: i32)
    let ui32s = binaryString(value: ui32)
    let i64s = binaryString(value: i64)
    let ui64s = binaryString(value: ui64)
    
    print("i8s = \(i8s)")
    print("ui8s = \(ui8s)")
    print("i16s = \(i16s)")
    print("ui16s = \(ui16s)")
    print("i32s = \(i32s)")
    print("ui32s = \(ui32s)")
    print("i64s = \(i64s)")
    print("ui64s = \(ui64s)")
}

testSingleVariables()
